call plug#begin(stdpath('data') . '/plug')

Plug 'editorconfig/editorconfig-vim'
Plug 'cocopon/iceberg.vim'
Plug 'godlygeek/tabular'
Plug 'tpope/vim-eunuch'
Plug 'sheerun/vim-polyglot'

call plug#end()
